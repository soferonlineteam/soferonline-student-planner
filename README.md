# Driving school planning generator

## Description
Organize the upcoming dirivng lessons and theoretical preparation (theory questions done in app & theory courses at school) needed by a driving school student, knowing the date of his/her theoretical exam.

[comment]: # (Constrained by:)
[comment]: # (	- current date)
[comment]: # (	- theoretical exam date)
[comment]: # (	- current student progress)
[comment]: # (		- all theory questions that have been done)
[comment]: # (	- date at which he/she desires to obtain the driving licence (optional))
[comment]: # (	- theory questions needed to be learn)
[comment]: # (		- based on all other students progress, find the best trajectory (order of questions))
[comment]: # (	- instructor schedule (other students, available hours))
[comment]: # (	- student preferences (e.g. some students might be at work between 9-17))

### Input:
	- theoretical exam date
	- (optional) practical exam date
	- category
	- current student progress
		- all theory questions done by current student
		- all theory courses attended
		- current theory course page (optional - might not be used)
		- done practical lessons (with instructor)
	- additional practical lessons needed
	- student preferences
		- avaiable hours, for each day of week (pairs startDate endDate)
		- unavailable hours, for exact date (pairs startDateTime endDateTime)
	- unavailable periods, due to external causes (e.g. holiday) (pairs startDateTime endDateTime)
	- instructor current schedule with available hours
		- unavailable periods, due to external causes (e.g. car needs mechanical revision) (pairs startDateTime endDateTime)	
	- professor (theory courses) schedule
		- unavailable periods, due to external causes (e.g. holiday) (pairs startDateTime endDateTime)	

	- Internal models:
		- best student theory trajectory: order of theory questions & course chapters:
			- minimize traning time
			- minimize mistakes
			- in concordance with theory lessons
			- most difficult questions -> should appear with a higher frequency
			-! this will combine with [current student progress - theory] to provide the questions

### Output:
	- driving lessons schedule with instructor
		- they must be evenly distributed, and not overlap with other of instructors constraints & hours
	- theory lessons schedule
		- must not overlap with restrictions (if possible)
	- daily theory preparation schedule
		- must be evenly distributed throughout next days
		- must also allow for more preparation in a day if wanted	
		- tasks to do every day; those can be
			- read course
			- do questionnaires
			- do some questions in PrepareMe mode
				- PrepareMe is an option on SoferOnline that recommends you the next best x(currently x = 40) questions to try, based on your progress
					- if this option is chosen, this algorithm should also return the recommended questions


## Other observations
- The output should be computed fast (under 1s)
- The theoretical recommendations will be recomputed every morning and displayed to the student

# Divide et impera
Splitting the big problem into two sub-problems:
* [P1] Organizarea orelor de condus pentru cursant
* [P2] Organizarea exercitiilor teoretice zilnice

## [P1] Organizarea orelor de condus pentru cursant
1 cursant 	: 30-40 ore - 1 luna - 2 luni
1 instructor: 8 ore/zi - 40 ore/saptamana - 800 - 1000 ore/luna
1 instructor: 20 cursanti / luna

### Usuall program for an instructor in a month
~10		 		cursanti / instructor / luna
15-20 		lectii(2h) / cursant (de obicei se fac in 2 luni)
160-240* 	ore de munca/instructor/luna => 80-120 time slots pt lectii
*Unii instructori au program de lucru mai lung, ei vor primi mai multi cursanti
Avem de repartizat 150-200 lectii in 160-240 time slot-uri in 2 luni

### Observations
La inscrierea unui cursant la scoala de soferi, se vor gasi time slot-uri pentru lectiile necesare.
Este foarte probabil ca instructorul sa aiba deja lectii programate.
Sistemul trebuie sa aleaga time slot-urile a.i. viitori cursanti inscrisi sa poata face lectii (sa nu se violeze constrangerile)

### Approach
#### Input
- training startDate, endDate
- category details (just relevant ones)
	- no of lessons
	- lesson duration
	- desired schooling period (the desired interval between first and last lesson)
- instructor schedule
	- available TS (time slots)
		- startDate, endDate (for empty schedule => ~240 time slots)
- student restrictions
	Hard: - unavailable between startDate - endDate	(*will be used for reducing TS)
	Soft: - preferred time of day (startHour, endHour)
- external restrictions (e.g. national event)	*can be soft or hard (startDate, endDate) * will further reduce TS

#### Output
- occupied time slots, with startDate - endDate


##### Should we use heuristic/evolutive/machine learning algorithms for this?
Can the search space be exhausted easily? => NO
Can a simple heuristic be used to solve this? => Maybe


### Summary
-[P1] Organizarea orelor de condus -
Diferit fata de problema din articol, asignarea lectiilor la anumite time slot-uri se face on demand:
cursantul se inscrie la scoala si îi sunt repartizate cele (de obicei) 15 lectii a cate 2h fiecare, pe parcursul a 2 luni, la un anumit instructor (la alegerea cursantului). Instructorul are deja alti cursanti in program, deci orele trebuie 'strecurate' in time slot-urile libere. 

Calcule
~10		 		cursanti / instructor / perioada de instruire (1-2 luni)
15-20 		lectii(2h) / cursant (de obicei se fac in 2 luni)
160-240* 	ore de munca/instructor/luna => 80-120 time slots pt lectii
*Unii instructori au program de lucru mai lung, ei vor primi mai multi cursanti

Spatiul de cautare este destul de mare, deci nu se poate aplica o metoda 'bruta'
Cred ca se poate gasi o euristica (cu Constraint Satisfaction) suficient de buna care sa repartizeze lectiile pe time-slot-uri (respectand hard constraints si optimizand soft constraints) pt un nou cursant. Euristica va lua in calcul:
- training startDate, endDate
- category details (just relevant ones)
	- no of lessons
	- lesson duration
	- desired schooling period (the desired interval between first and last lesson)
- instructor schedule
	- available TS (time slots)
		- startDate, endDate (for empty schedule => ~240 time slots)
- student restrictions
	Hard: - unavailable between startDate - endDate	(*will reduce available TSs)
	Soft: - preferred time of day (startHour, endHour)
- external restrictions (e.g. national event)	*can be soft or hard (startDate, endDate) * will further reduce TS

Pot sa folosesc aceasta metoda in rezolvarea P1 si sa o includ in lucrarea de disertatie?



## [P2] Organizarea exercitiilor teoretice zilnice
~1300 de intrebari / categorie
18 capitole
Dificultatea intrebarilor se calculeaza in functie de procentul global de raspunsuri gresite (*si eventual numarul de capitole din care face parte;)
In fiecare zi, cursantul va primi exercitii, prin push notifications, in functie de progresul lui actual si progresul global.
Exercitiile consta in chestionare generate cu anumite intrebari sau simple intrebari.
Daca face zilnic aceste exercitii, cursantul ar trebui sa fie 100% pregatit pentru examen, cu cateva zile (stabilit in prealabil) inainte de ziua examenului.
Daca sare peste una din zile, intrebarile se vor imparti pe zilele urmatoare.
In ultimele zile, va primi intrebari de recapitulare.


### Input
- progresul actual al cursantului: Lista de intrebari efectuate (id intrebare, raspuns dat)
	- lista intrebarilor efectuate astazi
- progresul global al tuturor cursantilor: same as above
- data examenului (data la care trebuie sa fie 100% pregatit)

### Output
- lista exercitiilor ce trebuie sa le mai efectueze astazi


### Summary
Actual, pentru P2 avem implementata o metoda ce recomanda cursantului intrebarile la care sa se pregateasca doar in functie de progresul lui si nu este raportata la timp. Oricand intri in platforma, iti va recomanda sa faci exercitii. Pentru aceasta ordonam intrebarile in functie de un rank pe capitole (prioritizam capitolele din care s-au efectuat mai putine intrebari si cele la care s-a gresit mai des). Pe viitor, functionalitatea descrisa de P2 va fi implementata in aplicatie.

Aici m-am gandit sa creez un sistem de ranking de dificultate pentru intrebari, in functie de instoricul global al intrebarilor efectuate. Acesta, impreuna cu istoricul cursantului actual (ultimul va avea o pondere mai mare) sa fie folosit in recomandarile zilnice.